package com.hurniakyaroslav.model;


import com.hurniakyaroslav.goods.Goods;

import java.util.ArrayList;

public interface Model {
    public ArrayList<Goods> getGoodsList();

    public void sortByClassName();

    public void addElement(Goods goods);

    public void removeAllElements();

    public ArrayList<Goods> findInPrice(Class Goods, int min, int max);

}

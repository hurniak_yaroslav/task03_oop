package com.hurniakyaroslav.model;

import com.hurniakyaroslav.goods.Goods;

import java.util.ArrayList;
import java.util.Comparator;

public class Hypermarket implements Model {
    private ArrayList<Goods> goodsList;

    public Hypermarket() {
        super();
        goodsList = new ArrayList<Goods>();
    }

    @Override
    public ArrayList<Goods> getGoodsList() {
        return goodsList;
    }

    @Override
    public void sortByClassName() {
        goodsList.sort(Comparator.comparingInt(Goods::getClassNumber));
    }

    @Override
    public void addElement(Goods goods) {
        goodsList.add(goods);
    }

    @Override
    public void removeAllElements() {
        goodsList.clear();
    }

    @Override
    public ArrayList<Goods> findInPrice(Class Goods, int min, int max) {
        ArrayList<Goods> goodsInPrice = new ArrayList<>();
        for (Goods g : goodsList) {
            if (g.getClass() == Goods && g.getPrice() > min && g.getPrice() < max) goodsInPrice.add(g);
        }
        return goodsInPrice;
    }

}

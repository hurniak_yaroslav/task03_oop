package com.hurniakyaroslav.view;

public interface View {
    public void show();
}

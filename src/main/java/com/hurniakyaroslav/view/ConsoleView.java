package com.hurniakyaroslav.view;

import com.hurniakyaroslav.controller.Controller;
import com.hurniakyaroslav.controller.Terminal;
import com.hurniakyaroslav.goods.Goods;
import com.hurniakyaroslav.goods.PaintProduct;
import com.hurniakyaroslav.goods.PlumbingGoods;
import com.hurniakyaroslav.goods.WoodenGoods;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class ConsoleView implements View {
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input;

    public ConsoleView() {
        controller = new Terminal();
        input = new Scanner(System.in);

        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - print list of all goods");
        menu.put("2", "  2 - update list");
        menu.put("3", "  3 - find goods");
        menu.put("4", "  4 - sort list");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
    }

    private void pressButton1() {
        for (Goods g : controller.getGoodsList()) {
            System.out.println(g);
        }
    }

    @Deprecated
    private void pressButton2() {
        try {
            controller.updateList();
        } catch (IOException e) {
            e.printStackTrace();
        }
        pressButton1();
        //try-catch is deprecated
    }

    private void pressButton3() {
        System.out.println("Please input number of goods`s type you want to find:");
        System.out.print("1-Wood 2-Paint 3-Plumbing: ");
        int typeNumber = Integer.parseInt(input.nextLine());
        System.out.print("Enter 2 numbers: price limits for search.");
        int minPrice = Integer.parseInt(input.nextLine());
        int maxPrice = Integer.parseInt(input.nextLine());
        switch (typeNumber) {
            case 1:
                System.out.println(controller.findInPrice(WoodenGoods.class, minPrice, maxPrice));
                break;
            case 2:
                System.out.println(controller.findInPrice(PaintProduct.class, minPrice, maxPrice));
                break;
            case 3:
                System.out.println(controller.findInPrice(PlumbingGoods.class, minPrice, maxPrice));
                break;
            default:
                System.out.println("Fail input! Try again =)");
                return;

        }
    }

    private void pressButton4() {
        controller.sortGoodsListByName();
    }

    //-------------------------------------------------------------------------

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }


    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }
}

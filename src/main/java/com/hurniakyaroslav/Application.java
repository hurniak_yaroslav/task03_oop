package com.hurniakyaroslav;

import com.hurniakyaroslav.view.ConsoleView;

/**
 * Main program class.
 */
class Application {
    /**
     *
     * @param args Additional parameters, which you can write in command line.
     */
    public static void main(final String[] args) {
        new ConsoleView().show();
    }
}

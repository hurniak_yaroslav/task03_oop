package com.hurniakyaroslav.goods;

public class PlumbingGoods extends Goods {
    public PlumbingGoods() {
        super();
    }

    private PlumbingGoodsEnum pge;

    public PlumbingGoods(final PlumbingGoodsEnum pge, final int price, final int weight) {
        super(price, weight);
        this.pge = pge;
    }

    @Override
    public String toString() {
        return super.toString() + "Type: " + pge;
    }
}

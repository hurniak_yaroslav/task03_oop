package com.hurniakyaroslav.goods;

/**
 * Abstract class.
 */
public abstract class Goods {
    /**
     * Goods`s price.
     */
    private int price;
    /**
     * Goods`s weight.
     */
    private int weight;

    public Goods() {
        price = 0;
        weight = 0;
    }

    /**
     *
     * @param extPrice price
     * @param extWeight weight
     */
    Goods(final int extPrice, final int extWeight) {
        price = extPrice;
        weight = extWeight;
    }


    @Override
    public String toString() {
        return this.getClass().getSimpleName()
                + " Price: " + price + " Weight: " + weight;
    }

    /**
     *
     * @return Goods`s class id.
     */
    public final int getClassNumber() {
        switch (this.getClass().getSimpleName()) {
            case "WoodenGoods":
                return 0;
            case "PlumbingGoods":
                return 1;
            case "PaintProducts":
                return 2;
            default:
                return 3;
        }
    }

    /**
     *
     * @return goods`s price.
     */
    public final int getPrice() {
        return price;
    }


}

package com.hurniakyaroslav.goods;

/**
 * Types of Paint Products.
 */
public enum PaintProductsType {
    /**
     * Lacquer.
     */
    LAQUER,
    /**
     * Paint.
     */
    PAINT,
    /**
     * Soil.
     */
    SOIL
}

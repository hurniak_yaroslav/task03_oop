package com.hurniakyaroslav.goods;

public class WoodenGoods extends Goods {

    private WoodenGoodsEnum woodenGoodsEnum;

    public WoodenGoods() {
        super();
    }

    public WoodenGoods(WoodenGoodsEnum wge, int extPrice, int extWeight) {
        super(extPrice, extWeight);
        woodenGoodsEnum = wge;
    }

    @Override
    public String toString() {
        return super.toString() + " Type: " + woodenGoodsEnum;
    }
}

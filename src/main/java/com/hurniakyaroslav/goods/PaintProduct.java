package com.hurniakyaroslav.goods;

/**
 * Class PaintProduct.
 */
public class PaintProduct extends Goods {
    /**
     * Super`s constructor
     */
    public PaintProduct() {
        super();
    }

    /**
     * Product type.
     */
    private PaintProductsType ppt;

    /**
     *
     * @param extppt Type of product
     * @param price Price of product
     * @param weight Weight of product.
     */
    public PaintProduct(final PaintProductsType extppt, final int price,
                        final int weight) {
        super(price, weight);
        this.ppt = extppt;
    }
}

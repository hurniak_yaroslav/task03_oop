package com.hurniakyaroslav.controller;

import com.hurniakyaroslav.goods.Goods;

import java.io.IOException;
import java.util.List;

/**
 * MVC controller.
 */
public interface Controller {

    /**
     *
     * @return List of Goods from Hypermarket.
     */
    List<Goods> getGoodsList();

    /**
     * Method
     */
    void sortGoodsListByName();

    /**
     * Method rewrite list of goods.
     * @throws IOException method use readFromFile method.
     */
    void updateList() throws IOException;

    /**
     *
     * @param goods  require Goods class.
     * @param min minimal limit of price.
     * @param max maximal limit of price.
     * @return list of goods in price limits.
     */
    List<Goods> findInPrice(Class goods, int min, int max);

    /**
     *
     * @throws IOException Method read goods info from file data.txt.
     */
    void readFromFile() throws IOException;



}

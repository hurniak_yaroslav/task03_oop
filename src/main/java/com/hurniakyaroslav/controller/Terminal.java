package com.hurniakyaroslav.controller;

import com.hurniakyaroslav.goods.Goods;
import com.hurniakyaroslav.goods.PaintProduct;
import com.hurniakyaroslav.goods.PaintProductsType;
import com.hurniakyaroslav.goods.PlumbingGoods;
import com.hurniakyaroslav.goods.PlumbingGoodsEnum;
import com.hurniakyaroslav.goods.WoodenGoods;
import com.hurniakyaroslav.goods.WoodenGoodsEnum;
import com.hurniakyaroslav.model.Hypermarket;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

public class Terminal implements Controller {

    /**
     * Composition to Hypermarket object.
     */
    private Hypermarket hypermarket;

    /**
     * Constructor without parametres.
     */
    public Terminal() {
        hypermarket = new Hypermarket();

    }

    @Override
    public final List<Goods> findInPrice(final Class goods,
                                         final int min, final int max) {
        return hypermarket.findInPrice(goods, min, max);
    }

    @Override
    public final List<Goods> getGoodsList() {
        return hypermarket.getGoodsList();
    }

    @Override
    public final void sortGoodsListByName() {
        hypermarket.sortByClassName();
    }


    @Override
    public final void updateList() throws IOException {
        hypermarket.removeAllElements();
        readFromFile();
    }

    @Override
    public final void readFromFile() throws IOException {
        File file = new File("data.txt");
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String line = null;

        try {
            String className = null;
            while ((line = reader.readLine()) != null) {
                String[] lineArray = line.split(" ");
                className = lineArray[0];

                switch (className) {

                    case "WoodenGoods":
                        WoodenGoodsEnum wge =
                                WoodenGoodsEnum.valueOf(lineArray[1]);
                        try {
                            hypermarket.addElement(new WoodenGoods(wge,
                                    Integer.valueOf(lineArray[2]),
                                    Integer.valueOf(lineArray[3])));

                        } catch (IllegalArgumentException e) {
                            e.printStackTrace();
                            System.out.println("Check file data!");
                        }
                        break;

                    case "PlumbingGoods":
                        PlumbingGoodsEnum pge =
                                PlumbingGoodsEnum.valueOf(lineArray[1]);
                        try {
                            hypermarket.addElement(new PlumbingGoods(pge,
                                    Integer.valueOf(lineArray[2]),
                                    Integer.valueOf(lineArray[3])));

                        } catch (IllegalArgumentException e) {
                            e.printStackTrace();
                            System.out.println("Check file data!");
                        }
                        break;


                    case "PaintProduct":
                        PaintProductsType pp =
                                PaintProductsType.valueOf(lineArray[1]);
                        try {
                            hypermarket.addElement(new PaintProduct(pp,
                                    Integer.valueOf(lineArray[2]),
                                    Integer.valueOf(lineArray[3])));

                        } catch (IllegalArgumentException e) {
                            e.printStackTrace();
                            System.out.println("Check file data!");
                        }
                        break;

                    default:
                        System.out.println("Wrong file line!");
                        break;
                }


            }

        } finally {
            reader.close();
        }
    }
}

